<?php
	session_start();
	include ('head.php');
	require('../inc/connectpdo.php');
    require('../inc/usersDAO.php');
    require('../inc/societeDAO.php');

    $societe = findSociete();
	$usersList=findUsers();
?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
             <?php include ('navhead.php'); ?>
            <?php include('navmenu.php');?>

          </div>
        </div>

       <?php include ('navtop.php'); ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Offer<small></small></h3>
              </div>

              
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Offer management</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal" role="form" name="form" action="../inc/insertoffer.php" method="post">
                        <div class="form-group">
                            <label for="societe" class="col-sm-4 col-md-2 control-label">Societe</label>
                            <div class="col-sm-20 col-md-8">
                                <select class="form-control" name="societe">
                                <?php foreach($societe as $soc){ ?>
                                    <option value="<?php echo $soc->id; ?>"><?php echo $soc->nom; ?> <?php echo $soc->lieu; ?></option>
                                <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="emploi" class="col-sm-4 col-md-2 control-label">Emploi</label>
                            <div class="col-sm-20 col-md-8">
                                <input type="text" name="emploi" class="form-control" id="emploi" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="descript" class="col-sm-4 col-md-2 control-label">Description</label>
                            <div class="col-sm-20 col-md-8">
                                <input type="text" name="descript" class="form-control" id="descript" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="salaire" class="col-sm-4 col-md-2 control-label">Salaire(Ar)</label>
                            <div class="col-sm-20 col-md-8">
                                <input type="number" name="salaire" class="form-control" id="salaire" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-20">
                                <button type="submit" class="btn btn-primary"><i class="icon icon-check icon-lg"></i>Valider</button>
                            </div>
                        </div>
                    </form>
                  </div>
                  <div class="x_title">
                    <h2>Society management</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form class="form-horizontal" name="societe" role="form" action = "../inc/addSociete.php" method="POST">
                        <div class="form-group">
                            <label for="nom" class="col-sm-4 col-md-2 control-label">Societe</label>
                            <div class="col-sm-20 col-md-8">
                                <input type="text" name="nom" class="form-control" id="nom" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lieu" class="col-sm-4 col-md-2 control-label">Lieu</label>
                            <div class="col-sm-20 col-md-8">
                                <input type="text" name="lieu" class="form-control" id="lieu" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-20 col-md-8">
                                <button type="submit" class="btn btn-primary"><i class="icon icon-check icon-lg"></i>Inserer</button>
                            </div>
                        </div>
                    </form>
                  </div>

                </div>
              </div>
            </div>
          </div>
          
        </div>
        <!-- /page content -->

        <?php include('footer.php'); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>

    <script src="../build/js/custom.min.js"></script>

  </body>
</html>