<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login/Sign-In</title>
  

<link rel="stylesheet" href="../assets/css/cssLogin/css/style.css">
<script type="text/javascript">
	function checkPasswordMatch() {
    var password = $("#user-pw").val();
    var confirmPassword = $("#user-pw-repeat").val();

    if (password != confirmPassword){
        $("#divCheckPasswordMatch").html("Passwords do not match!");
        document.getElementById("buttonCreate").style.display = "none";
    }
    else{
        $("#divCheckPasswordMatch").html("Passwords match.");
        document.getElementById("buttonCreate").style.display = "block";
  }
}
</script>
  
</head>

<body style="background-image: url('../assets/images/background1.jpg'); width: 100%; height: 150px;">
  <div class="logmod">
  <div class="logmod__wrapper">
    <a href="home.php"><span class="logmod__close">Close</span></a>
    <div class="logmod__container">
      <ul class="logmod__tabs">
        <li data-tabtar="lgm-2"><a href="#">Login</a></li>
        <!--<li data-tabtar="lgm-1"><a href="#">Sign Up</a></li>-->
      </ul>
      <div class="logmod__tab-wrapper">
      <div class="logmod__tab lgm-1">
        <div class="logmod__heading">
          <span class="logmod__heading-subtitle">Enter your personal details <strong>to create an acount</strong></span>
        </div>
        <div class="logmod__form">
          <form accept-charset="utf-8" action="addUser.php" class="simform" method="post">
		  <div class="sminputs">
              <div class="input string optional">
                <label class="string optional">Name*</label>
                <input class="string optional" maxlength="255" id="user-name" name="name" placeholder="Enter your name here" type="text" size="50" required/>
              </div>
              <div class="input string optional">
                <label class="string optional">Surname*</label>
                <input class="string optional" maxlength="255" id="user-surname" name="surname" placeholder="Enter your surname here" type="text" size="50" required/>
              </div>
            </div>
			<div class="sminputs">
              <div class="input string optional">
                <label class="string optional">Cell Number*</label>
                <input class="string optional" maxlength="255" id="user-number" name="number" placeholder="Enter your phone number here" type="text" size="50" required/>
              </div>
            </div>
            <div class="sminputs">
              <div class="input full">
                <label class="string optional" for="user-name">Email*</label>
                <input class="string optional" maxlength="255" id="user-email" name="email" placeholder="Enter your email here" type="email" size="50" required/>
              </div>
            </div>
            <div class="sminputs">
              <div class="input string optional">
                <label class="string optional" for="user-pw">Password *</label>
                <input class="string optional" maxlength="255" id="user-pw" name="password" placeholder="Enter your password here" type="password" size="50" required/>
              </div>
              <div class="input string optional">
                <label class="string optional" for="user-pw-repeat">Repeat password *</label>
                <input class="string optional" maxlength="255" id="user-pw-repeat" onkeyup="checkPasswordMatch();" placeholder="Repeat your password here" type="password" size="50" required/>
              </div>
			  <div align="center" class="registrationFormAlert" id="divCheckPasswordMatch"></div>
            </div>
            <div id="buttonCreate" class="simform__actions">
              <input class="sumbit" name="commit" type="submit" value="Create Account"/>
              <span class="simform__actions-sidetext">Your information will be registered</span>
            </div> 
          </form>
        </div>
      </div>
      <div class="logmod__tab lgm-2">
        <div class="logmod__heading">
          <span class="logmod__heading-subtitle">Enter your email and password <strong>to sign in</strong></span>
        </div> 
        <div class="logmod__form">
          <form accept-charset="utf-8" action="check_login.php" class="simform" method="post">
            <div class="sminputs">
              <div class="input full">
                <label class="string optional" for="user-name">Email*</label>
                <input class="string optional" maxlength="255" id="email" name="email" placeholder="Enter your email here" type="email" size="50" required/>
              </div>
            </div>
            <div class="sminputs">
              <div class="input full">
                <label class="string optional" for="user-pw">Password *</label>
                <input class="string optional" maxlength="255" id="pw" name="password" placeholder="Enter your password here" type="password" size="50" required/>
                						<span class="hide-password">Show</span>
              </div>
            </div>
            <div class="simform__actions">
              <input class="sumbit" name="commit" type="submit" value="Log In" />
            </div> 
          </form>
        </div>
          </div>
      </div>
    </div>
  </div>
</div>
<script src='../assets/js/jquery.min.js'></script>
<script src="../assets/css/cssLogin/js/index.js"></script>

</body>
</html>
