<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<!-- Core JavaScript Files -->
    <script src="../assets/js/jquery.min.js"></script>	 
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery.easing.min.js"></script>
	<script src="../assets/js/wow.min.js"></script>
	<script src="../assets/js/jquery.scrollTo.js"></script>
	<script src="../assets/js/jquery.appear.js"></script>
	<script src="../assets/js/stellar.js"></script>
	<script src="../assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
	<script src="../assets/js/owl.carousel.min.js"></script>
	<script src="../assets/js/nivo-lightbox.min.js"></script>
    <script src="../assets/js/custom.js"></script>