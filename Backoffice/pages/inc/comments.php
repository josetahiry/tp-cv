<?php
    class Comments{
        private $_id;
        private $_idusers;
        private $_title;
        private $_content;
        private $_rating;
        
        public function __construct($id,$idu,$title,$content,$rating){
            $this->_id = $id;
            $this->_idusers = $idu;
            $this->_title = $title;
            $this->_content = $content;
        }

        public function set_id($id){
            $this->_id = $id;
        }

        public function set_idusers($idu){
            $this->_idusers = $idu;
        }

        public function set_title($title){
            $this->_title = $title;
        }

        public function set_content($content){
            $this->_content = $content;
        }

        public function get_id(){
            return $this->_id ;
        }

        public function get_idusers(){
            return $this->_idusers ;
        }

        public function get_title(){
            return $this->_title ;
        }

        public function get_content(){
            return $this->_content ;
        }
    }
?> 