<?php
	require('../inc/usersDAO.php');
?>

<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="top-area">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
					<p class="bold text-left">Monday - Saturday, 8am to 10pm </p>
					</div>
					<div class="col-sm-6 col-md-6">
					<?php
						$includelogin[0]="<p class='bold text-right'><a href='login.php'>Sign in / Sign up</a></p>";
						if(!isset($_SESSION["id"])){
							echo $includelogin[0];
						}
						else{
							$userConnect = findUsersById($_SESSION["id"]);
							foreach($userConnect AS $user){
								$nameUserConnect = $user->nom;
								$includelogin[1]="<p class='bold text-right'>Bonjour Mr(Mme) $nameUserConnect || <a href='deconnexion.php'> Deconnexion</a></p>";
								echo $includelogin[1];
							}
						}
					?>
					</div>
				</div>
			</div>
		</div>
        <div class="container navigation">
		
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="home.php">
                    <img src="../assets/img/logo.png" alt="" width="150" height="40" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
			  <ul class="nav navbar-nav">
				<li class="active"><a href="#intro">Home</a></li>
				<li><a href="#service">Service</a></li>
				<li><a href="#doctor">Doctors</a></li>
				<li><a href="#facilities">Facilities</a></li>
				<li><a href="#testimonial">Comments</a></li>
			  </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>