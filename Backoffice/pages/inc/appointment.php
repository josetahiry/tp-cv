<?php
    class Appointments{
        private $_id;
        private $_daty;
        private $_hour;
        private $_iddoctors;
        private $_iduser;

        public function __construct($id,$daty,$hour,$idd,$iduser){
            $this->_id = $id;
            $this->_daty = $daty;
            $this->_hour = $hour;
            $this->_iddoctors = $idd;
            $this->_iduser = $iduser;
        }

        public function set_id($id){
            $this->_id = $id;
        }

        public function set_daty($daty){
            $this->_daty = $daty;
        }

        public function set_hour($hour){
            $this->_hour = $hour;
        }

        public function set_iddoctors($idd){
            $this->_iddoctors = $idd;
        }
		
		public function set_iduser($iduser){
            $this->_iduser = $iduser;
        }

        public function get_id(){
            return $this->_id;
        }

        public function get_daty(){
            return $this->_daty;
        }

        public function get_hour(){
            return $this->_hour;
        }

        public function get_iddoctors(){
            return $this->_iddoctors;
        }
		
		public function get_iduser(){
            return $this->_iduser;
        }
    }
?>