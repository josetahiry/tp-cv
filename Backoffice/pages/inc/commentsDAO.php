<?php
    require('comments.php');

    function findComments(){
		$connexion = getmysql();
		try{
            $resultats1=$connexion->query("select * from comments"); // on va chercher tous les membres de la table qu'on trie par ordre croissant
            $resultats1->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet
            $result = array();
            while( $ligne = $resultats1->fetch() ) {
                $result[] = $ligne;
            }
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
        $connexion = null;
        return $result;
    }
	
	function findCommentsWithPhoto(){
		$connexion = getmysql();
		try{
            $resultats1=$connexion->query("select comments.id,comments.idusers,comments.title,comments.content,comments.rating,users.photo from comments join users on comments.idusers = users.id");
            $resultats1->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats1->fetch() ) {
                $result[] = $ligne;
            }
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
        $connexion = null;
        return $result;
    }

    function insertComments($comments){
        $connexion = getmysql();
        try{
			$connexion->exec("insert into comments values('','".
                $comments->get_idusers()."','".
                $comments->get_title()."','".
                $comments->get_content()."')"
            );
            $connexion->exec("commit");
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		} 
        $connexion = null;
    }

?>