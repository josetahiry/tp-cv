<footer>
	
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<h5>About Medical Center</h5>
						<p>
						...
						</p>
					</div>
					</div>
					<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<h5><a href="team.php">Team</a></h5>
					</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<h5>Contact</h5>
						<p><b>Call number :</b> +261 32 78 822 35 / <b>Mail :</b> medicalcenter@gmail.com</p>		
					</div>
					</div>
					<div class="wow fadeInDown" data-wow-delay="0.1s">
						<div class="widget">
							<h5>Follow us</h5>
							<ul class="company-social">
									<li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
									<li class="social-dribble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>	
		</div>
		<div class="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="wow fadeInLeft" data-wow-delay="0.1s">
					<div class="text-left">
					<p>&copy;Copyright - Medical Center Website.</p>
					</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="wow fadeInRight" data-wow-delay="0.1s">
					<div class="text-right">
						<div class="credits">
                            <a href="#">Medical Center Application</a> by <a href="team.php">Team Potatoes</a>
                        </div>
					</div>
					</div>
				</div>
			</div>	
		</div>
		</div>
	</footer>