<?php
  session_start();
  if(empty($_SESSION['user'])){
    header('location:login.php');
  }
  require('../inc/functions/function.php');
  $apropos = getCatApropos();
  include ('head.php');
?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <?php include('navhead.php') ?>
            <?php include('navmenu.php')?>
          </div>
        </div>
<?php include ('navtop.php'); ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">
                <div class="col-md-12 col-sm-12 col-xs-12 bg-white">
                  <div class="x_title">
                    <h3 align="center">Content management</h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-6">
                    <div>
                      <div class="">
                        <form role = "form" name = "formsection" action = "../inc/modifycontent.php" method = "POST">
                          <div class="form-group">
                            <select class="form-control" name="section">
                            <?php foreach($apropos as $ap){ ?>
                              <option value="<?php echo $ap->id; ?>"><?php echo $ap->nom; ?></option>
                            <?php } ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <textarea class="ckeditor" name="editor"></textarea>
                          </div>
                          <div class="form-group">
                            <p align="center"><input type = "submit" value = "Modifier" class = "btn btn-success"></p>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <hr>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <br />
          <div class="row">
          </div>
          </div>
        </div>
        <!-- /page content -->
<?php include('footer.php'); ?>
</div>
    </div>
    <!--CKeditor-->
    <script type="text/javascript" src="../assets/ckeditor/ckeditor.js"></script>
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
	
       
  </body>
</html>
