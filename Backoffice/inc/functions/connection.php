<?php
    if(isset($_POST['email']) & isset($_POST['pass'])){
        require('function.php');
        $email = $_POST['email'];
        $pass = $_POST['pass'];
        $result = connexion($email, $pass);
        if($result != null){
            session_start();
            $_SESSION['user'] = $result;
            header('Location:../../pages/page-careers.php');
        }else if(checkexist(($email)) == 1){
            header('Location:../../pages/page-log-in.php?status=1');
        }else{
            header('Location:../../pages/page-log-in.php?status=2');
        }
    }else{
        header('Location:../../pages/page-log-in.php');
    }
?>