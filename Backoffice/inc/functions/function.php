<?php
    require('connectmysql.php');

    function connexion($email, $pass){
        $result = array();
        $conn = null;
        $Res = null;
		try{
			$conn = getmysql();
			$Res = $conn->query(sprintf("select id from user where email='%s' and pass=sha1('%s')",$email,$pass));
			$Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result = $ligne->id;
			}
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function connexionAdmin($email, $pass){
        $result = null;
        $conn = null;
        $Res = null;
		try{
            $conn = getmysql();
			$Res = $conn->query(sprintf("select id from user where email='%s' and pass=sha1('%s') and admin = 1",$email,$pass));
			$Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result = $ligne->id;
			}
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function checkexist($email){
        $result = array();
        $conn = null;
        $Res = null;
		try{
			$conn = getmysql();
			$Res = $conn->query("select * from user where email='".$email."'");
			$Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result = $ligne;
			}
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
	}
    
    function inscription($nom, $prenom, $email, $pass){
		inscriptionRem($nom, $prenom, $email, $pass, null);
	}

    function inscriptionRem($nom, $prenom, $email, $pass, $remark){
		try{
            $conn = getmysql();
			$Res = $conn->query(sprintf("insert into user(nom,prenom,email,pass,remarque,actif,admin) values('%s','%s','%s',sha1('%s'),'%s',1,0)",$nom,$prenom,$email,$pass,$remark));
            $Res->setFetchMode(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
    }
    
    function findHomeDetail(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from texte where idcategory = 1");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function findStudies(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from studies");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function findStudiesById($id){
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query(sprintf("select * from studies where id = '%s'",$id));
            $Res->setFetchMode(PDO::FETCH_OBJ);
			if( $ligne = $Res->fetch() ){
				$result = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function findParcours(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from parcours");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function finduserById($id){
        $result = null;
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query(sprintf("select * from user where id = '%s'",$id));
            $Res->setFetchMode(PDO::FETCH_OBJ);
			if( $ligne = $Res->fetch() ){
				$result = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function findParcoursById($id){
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query(sprintf("select * from parcours where id = '%s'",$id));
            $Res->setFetchMode(PDO::FETCH_OBJ);
			if( $ligne = $Res->fetch() ){
				$result = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }
    
    function findHobbies(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from hobbies");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function getApropos(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from apropos");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function getCatApropos(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from categoryapropos");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function updateContent($idcat, $content){
        try{
            $conn = getmysql();
			$Res = $conn->query(sprintf("update apropos set content = '%s' where idcategory = '%s'",$content, $idcat));
            $Res->setFetchMode(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
    }

    function delParcour($id){
        try{
            $conn = getmysql();
			$Res = $conn->query(sprintf("delete from parcours where id = '%s'",$id));
            $Res->setFetchMode(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
    }

    function insertParcour($nom, $debut, $fin, $description){
        try{
            $conn = getmysql();
			$Res = $conn->query(sprintf("insert into parcours(nom,datyd,datyf,descript,remarque) values('%s','%s','%s','%s','')",$nom, $debut, $fin, $description));
            $Res->setFetchMode(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
    }

?>