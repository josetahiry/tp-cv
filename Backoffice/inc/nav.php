<?php
	require('../inc/usersDAO.php');
?>

<style type="text/css">
#search {
	margin-top:10px;
	margin-left:20px;
	border: none;   /* Supprime les bordures par défaut */
	color: #111;
	border: none;   /* Supprime les bordures par défaut */
	border-bottom-style: solid;   /* Style de la bordure */
	border-bottom-width: 1px;   /* Epaisseur de la bordure */
	border-bottom-color: #111   /* Couleur de la bordure */
}
#search-btn {
	color: #429398;   /* Couleur du texte du bouton valider */
	letter-spacing: 1px;   /* Espacement des caractères du texte du bouton valider */
	font-family: 'Lato', sans-serif;   /* Police du texte du bouton valider */
	font-style: normal;   /* Style de la police du texte du bouton valider : normal = normal ; italic = italique */
	font-weight: normal;   /* Graisse du texte du bouton valider : normal = normal ; bold = gras */
	font-size: 12px;   /* Taille de la police du texte du bouton valider */
	text-transform: uppercase;   /* Texte en majuscules */
	border: none;   /* Pas de bordure au bouton valider */
	padding: 5px 10px 5px 10px;   /* Espace entre le texte et le bord du bouton : haut droite bas gauche */
}
#search-btn:hover {
	background-color: #429398;   /* Couleur de fond */
	color: #fff;   / * Couleur de la police */
}
</style>

<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="top-area">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
					<p class="bold text-left">Monday - Saturday, 8am to 10pm </p>
					</div>
					<div class="col-sm-6 col-md-6">
					<?php
						$includelogin[0]="<p class='bold text-right'><a href='login.php'>Sign in / Sign up</a></p>";
						if(!isset($_SESSION["id"])){
							echo $includelogin[0];
						}
						else{
							$userConnect = findUsersById($_SESSION["id"]);
							foreach($userConnect AS $user){
								$nameUserConnect = $user->nom;
								$includelogin[1]="<p class='bold text-right'>Bonjour Mr(Mme) $nameUserConnect || <a href='deconnexion.php'> Deconnexion</a></p>";
								echo $includelogin[1];
							}
						}
					?>
					</div>
				</div>
			</div>
		</div>
        <div class="container navigation">
		
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="home.php">
                    <img src="../assets/img/logo.png" alt="" width="150" height="40" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
			  <ul class="nav navbar-nav">
				<li class="active"><a href="#intro">Home</a></li>
				<li><a href="#service">Service</a></li>
				<li><a href="#doctor">Doctors</a></li>
				<li><a href="#facilities">Facilities</a></li>
				<li><a href="#testimonial">Comments</a></li>
				<li><form action="home.php?search=1#doctor" id="searchthis" method="post">
				<input id="search" type="text" name="search" placeholder="Search a doctor"/>
				<input id="search-btn" type="submit" value="Search" />
				</form></li>
			</form>
			</form></br>
			  </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>