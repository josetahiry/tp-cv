<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	
    <!-- css -->
    <link href="../assets/css/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../assets/plugins/cubeportfolio/css/cubeportfolio.min.css">
	<link href="../assets/css/css/nivo-lightbox.css" rel="stylesheet" />
	<link href="../assets/css/css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/css/owl.carousel.css" rel="stylesheet" media="screen" />
    <link href="../assets/css/css/owl.theme.css" rel="stylesheet" media="screen" />
	<link href="../assets/css/css/animate.css" rel="stylesheet" />
    <link href="../assets/css/css/style.css" rel="stylesheet">

	<!-- boxed bg -->
	<link id="bodybg" href="../assets/css/bodybg/bg1.css" rel="stylesheet" type="text/css" />
	<!-- template skin -->
	<link id="t-colors" href="../assets/css/color/default.css" rel="stylesheet">
    
	<style type="text/css">
		html { height: 100% }
		body { height: 100%; margin: 0; padding: 0 }
			  #carteId{ height: 100% }
		</style>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi9XIJ4QQAcos7rVuFHljY-NWCd5NUCrY&callback=initMap"
  type="text/javascript"></script>
		<script type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?sensor=false">
		</script>
		<script type="text/javascript">
		function initialize() {
		var mapOptions = {
		center: new google.maps.LatLng(-18.9193583,47.5190281),
		zoom: 17,
		mapTypeId:google.maps.MapTypeId.ROADMAP
				};
		var carte = new google.maps.Map(document.getElementById("carteId"),
		mapOptions);
		
		var location = new google.maps.LatLng(-18.9193583,47.5190281);
		
		var marker = new google.maps.Marker({
			position: location,				
			map: carte
		});
		
			  }
		google.maps.event.addDomListener(window, 'load', initialize);
				</script>