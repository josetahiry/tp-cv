<?php
    require('connectmysql.php');

    function connexion($email, $pass){
        $result = array();
        $conn = null;
        $Res = null;
		try{
			$conn = getmysql();
			$Res = $conn->query(sprintf("select id from user where email='%s' and pass=sha1('%s')",$email,$pass));
			$Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result = $ligne->id;
			}
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function checkexist($email){
        $result = array();
        $conn = null;
        $Res = null;
		try{
			$conn = getmysql();
			$Res = $conn->query("select * from user where email='".$email."'");
			$Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result = $ligne;
			}
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
	}
    
    function inscription($nom, $prenom, $email, $pass){
		inscriptionRem($nom, $prenom, $email, $pass, null);
	}

    function inscriptionRem($nom, $prenom, $email, $pass, $remark){
		try{
            $conn = getmysql();
			$Res = $conn->query(sprintf("insert into user(nom,prenom,email,pass,remarque,actif,admin) values('%s','%s','%s',sha1('%s'),'%s',1,0)",$nom,$prenom,$email,$pass,$remark));
            $Res->setFetchMode(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
    }
    
    function findHomeDetail(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from texte where idcategory = 1");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function findStudies(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from studies");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function findStudiesById($id){
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query(sprintf("select * from studies where id = '%s'",$id));
            $Res->setFetchMode(PDO::FETCH_OBJ);
			if( $ligne = $Res->fetch() ){
				$result = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function findParcours(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from parcours");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function finduserById($id){
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query(sprintf("select * from user where id = '%s'",$id));
            $Res->setFetchMode(PDO::FETCH_OBJ);
			if( $ligne = $Res->fetch() ){
				$result = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function findParcoursById($id){
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query(sprintf("select * from parcours where id = '%s' order by datyd asc",$id));
            $Res->setFetchMode(PDO::FETCH_OBJ);
			if( $ligne = $Res->fetch() ){
				$result = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }
    
    function getApropos(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from apropos");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

    function findHobbies(){
        $result = array();
        $conn = null;
        $Res = null;
        try{
			$conn = getmysql();
			$Res = $conn->query("select * from hobbies");
            $Res->setFetchMode(PDO::FETCH_OBJ);
			while( $ligne = $Res->fetch() ){
				$result[] = $ligne;
            }
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
        }
        $Res->CloseCursor();
        $conn = null;
        return $result;
    }

?>