<div class="znav-container" id=&"znav-container">
    <div class="container">
        <nav class="navbar navbar-toggleable-md">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger hamburger--emphatic">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </div>
            </button>
            <a class="navbar-brand" href="accueil">
                <h5><?php echo $apropos[1]->content; ?> <?php echo $apropos[0]->content; ?></h5>
            </a>
            <ul id="navlist">
                <li id="home"><a href="../pages/accueil"></a></li>
                <li id="study"><a href="../pages/formations"></a></li>
                <li id="exp"><a href="../pages/experience"></a></li>
            </ul>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                
                <ul class="navbar-nav ml-auto">
                    <li>
                        <a href="accueil">Accueil</a>
                    </li>
                    <li>
                        <a href="formations">Formations</a>
                    </li>
                    <li>
                        <a href="experience">Expériences</a>
                    </li>
                    <li>
                        <a href="contact">Contact</a>
                    </li>
                    <li>
                        <?php if(empty($_SESSION["user"])){ ?>
                            <a href="connexion">Se connecter</a>
                        <?php } else { ?>
                            <a href="../inc/functions/disconnect.php">Se déconnecter</a>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

<style>
    #navlist {
        position: relative;
    }

    #navlist li {
        margin: 0;
        padding: 0;
        list-style: none;
        position: absolute;
        top: 0;
    }

    #navlist li, #navlist a {
        height: 30px;
        display: block;
    }

    #home {
        left: 0px;
        width: 33px;
        background: url('../assets/images/sprite/web_icons.png') 0 0;
    }

    #study {
        left: 35px;
        width: 33px;
        background: url('../assets/images/sprite/web_icons.png') -33px 0;
    }

    #exp {
        left: 70px;
        width: 33px;
        background: url('../assets/images/sprite/web_icons.png') -66px 0;
    }
</style>

</div>