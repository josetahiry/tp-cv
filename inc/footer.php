<footer class="background-1 color-8 py-3 fs--1 fw-600 font-heading">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-auto font-1">&copy; 2018 &trade; Inc. </br> <?php echo $apropos[3]->content; ?> </div>
            
            
        </div>
    </div>
</footer>