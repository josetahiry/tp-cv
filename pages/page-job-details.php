<?php
    session_start();
    require('../inc/functions/function.php');
    $parcours = findParcours();
    $hobbies = findHobbies();
    $apropos = getApropos();
?>

<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryposh/page-job-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 13:29:14 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  -->
    <!--    Document Title-->
    <!-- =============================================-->
    <title>Experience | Jose Tahiry Rakotondrazafy</title>
    <!--  -->
    <!--    Favicons-->
    <!--    =============================================-->
    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../assets/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="details de l'experience professionnelle, des centres d'interêts et de loisirs de José Tahiry Rakotondrazafy">
    <meta name="keywords" content="CV,José Tahiry,Develloper,Programmer,Experience">
    <meta name="author" content="José Tahiry">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--  -->
    <!--    Stylesheets-->
    <!--    =============================================-->
    <!-- Default stylesheets-->
    <link rel="stylesheet" href="../assets/concat-css/" type="text/css" media="screen">
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
        <?php include('../inc/menu.php'); ?>
        <!-- /.znav-container-->
        <section class="pb-5">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h1 class="fs-4 fw-400">Experience professionnelle</h1>
                        <hr class="color-9 mt-5">
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <section class="font-1 pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Debut</th>
                                    <th>Fin</th>
                                    <th>Nom</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($parcours as $p){ ?>
                                <tr>
                                    <td><?php echo $p->datyd; ?></td>
                                    <td><?php echo $p->datyf; ?></td>
                                    <td><?php echo $p->nom; ?></td>
                                    <td><?php echo $p->descript; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <hr class="color-9 mt-5">
        <section class="pb-5">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 class="fs-4 fw-400">Loisirs</h2>
                        <hr class="color-9 mt-5">
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <section class="font-1 pt-0">
            <div class="container">
                <div class="row">
                    <ul>
                        <?php foreach($hobbies as $hob){ ?>
                        <li><?php echo $hob->nom; ?></li>
                        <?php } ?>
                    </ul>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <?php include('../inc/footer.php'); ?>
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <script src="../assets/lib/jquery/dist/jquery.js"></script>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="../assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Hotjar Tracking Code for http://markup.themewagon.com/tryposh-->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {
                hjid: 588723,
                hjsv: 5
            };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-76729372-3', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="../assets/js/core.js"></script>
    <script src="../assets/js/main.js"></script>
</body>

</html>