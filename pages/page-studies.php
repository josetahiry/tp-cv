<?php
    session_start();
    require('../inc/functions/function.php');
    $studies = findStudies();
    $apropos = getApropos();
?>

<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryposh/portfolio-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 13:30:56 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  -->
    <!--    Document Title-->
    <!-- =============================================-->
    <title>Etudes | Jose Tahiry Rakotondrazafy </title>
    <!--  -->
    <!--    Favicons-->
    <!--    =============================================-->
    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../assets/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="La liste des études parcourus par José Tahiry Rakotondrazafy">
    <meta name="keywords" content="CV,José Tahiry,Develloper,Programmer,Etudes">
    <meta name="author" content="José Tahiry">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--  -->
    <!--    Stylesheets-->
    <!--    =============================================-->
    <!-- Default stylesheets-->
    <link rel="stylesheet" href="../assets/concat-css/" type="text/css" media="screen">
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
        <?php include('../inc/menu.php'); ?>
        <!-- /.znav-container-->
        <section class="portfolio text-center font-1">
            <div class="container">
                <h1 class="fs-4">Etudes parcourues</h1>
                <p class="lead color-5">Voici la liste des etudes de José Tahiry Rakotondrazafy</p>
                <hr class="short color-7">
                <div class="sortable mt-6">
                    <div class="row sortable-container text-left">
                        <?php foreach($studies as $st){ ?>
                        <div class="sortable-item col-lg-6 animation">
                            <a href="page-studies-details.php?id=<?php echo $st->id ?>">
                                <div class="hoverbox">
                                    <div class="background-holder overlay" style="background-image:url(../assets/images/<?php echo $st->image ?>);"></div>
                                    <!--/.background-holder-->
                                    <div class="pt-11 pl-4 pb-3">
                                        <h5 class="mt-3">
                                            <?php
                                                $clean = $st->nom;
                                                $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
                                                $clean = strtolower($clean);
                                                $clean = preg_replace("/[\/_|+ -]+/", "-", $clean);
                                            ?>
                                            <a class="color-white" href="detail-etudes-<?php echo $st->id; ?>-<?php echo $clean; ?>" ><?php echo $st->datyd ?>-<?php echo $st->datyf ?>: <?php echo $st->nom ?></a>
                                        </h5>
                                        <p class="color-white"><?php echo $st->descript ?></p>
                                    </div>
                                </div>
                            </a>
                            <div class="my-4"></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <?php include('../inc/footer.php'); ?>
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <script src="../assets/lib/jquery/dist/jquery.js"></script>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="../assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Hotjar Tracking Code for http://markup.themewagon.com/tryposh-->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {
                hjid: 588723,
                hjsv: 5
            };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-76729372-3', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="../assets/lib/semantic-ui-dropdown/dropdown.js"></script>
    <script src="../assets/lib/semantic-ui-transition/transition.js"></script>
    <script src="../assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="../assets/js/core.js"></script>
    <script src="../assets/js/main.js"></script>
</body>
</html>