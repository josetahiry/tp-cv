<?php 
    session_start();
    require('../inc/functions/function.php');
    $details = findHomeDetail();
    $apropos = getApropos();
?>
<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryposh/page-careers.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 13:29:14 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  -->
    <!--    Document Title-->
    <!-- =============================================-->
    <title>Jose Tahiry Rakotondrazafy | Developper, Web maker & designer, Data scientist</title>
    <!--  -->
    <!--    Favicons-->
    <!--    =============================================-->
    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicons/favicon-16x16.png">
    <meta charset="UTF-8">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../assets/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="Curriculum Vitae (José Tahiry Rakotondrazafy)">
    <meta name="keywords" content="CV,José Tahiry,Develloper,Programmer">
    <meta name="author" content="José Tahiry">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--  -->
    <!--    Stylesheets-->
    <!--    =============================================-->
    <!-- Default stylesheets-->
    <link rel="stylesheet" href="../assets/concat-css/" type="text/css" media="screen">
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
        <?php include('../inc/menu.php'); ?>
        <!-- /.znav-container-->
        <section class="p-0 color-white font-1">
            <div class="background-holder overlay overlay-1" style="background-image:url(../assets/images/careers-header.jpg);"></div>
            <!--/.background-holder-->
            <div class="container">
                <div class="row color-white">
                    <div class="col-md-8 py-10">
                        <h1 class="fs-2"><?php echo $apropos[1]->content; ?> <?php echo $apropos[0]->content; ?></h1>
                        <h2 class="fs-1">ETU000517</h2>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <section class="background-0 font-1 pb-0">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-md-9 mt-4">
                        <h2 class="font-1 fs-2 fw-300 color-5">Developper, Web maker & designer, Data scientist</h2>
                        <hr class="mt-8 color-9">
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <section class="background-0 font-1">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-md-8">
                        <h3>Profil</h3>
                        <p class="font-1 color-6 fw-300"><img style="-moz-border-radius:50px; -webkit-border-radius:50px; border-radius:50px;" width="360px" src="../assets/images/profile/pdp.jpg" alt="Profile photo José Tahiry Rakotondrazafy"></p>
                    </div>
                </div>
                <div class="row mt-8">
                    <!-- Details -->
                    <?php foreach($details as $detail){ ?>
                        <a class="col-lg-6 col-xl-4 font-1 color-1 mb-4">
                            <div class="border color-9 p-5">
                                <div class="font-1 fs-2 fw-100 color-8 d-inline-block"><?php echo $detail->content; ?></div>
                                <h5 class="color-1 mb-0"><?php echo $detail->descript; ?></h5>
                                <h6 class="color-7 d-inline-block"><?php echo $detail->remarque; ?></h6>
                            </div>
                        </a>
                    <?php } ?>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <section class="py-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mb-4">
                        <img src="../assets/images/generic-06.jpg" alt="image generic developpeur">
                    </div>
                    <div class="col-lg-6 mb-4">
                        <img src="../assets/images/header-03.jpg" alt="image header developpeur">
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <?php include('../inc/footer.php'); ?>
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <script src="../assets/lib/jquery/dist/jquery.js"></script>
    
    <script src="../assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Hotjar Tracking Code for http://markup.themewagon.com/tryposh-->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {
                hjid: 588723,
                hjsv: 5
            };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-76729372-3', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="../assets/js/core.js"></script>
    <script src="../assets/js/main.js"></script>
</body>
<!-- Mirrored from markup.themewagon.com/tryposh/page-careers.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 13:29:14 GMT -->

</html>