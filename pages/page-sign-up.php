<?php
    require('../inc/functions/function.php');
    $status = "";
    if(isset($_GET['status'])){
        $status = "Erreur lors de l'inscription";
    }
    $apropos = getApropos();
?>

<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryposh/page-log-in.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 16:07:35 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  -->
    <!--    Document Title-->
    <!-- =============================================-->
    <title>Inscription | Inscrivez-vous ici</title>
    <!--  -->
    <!--    Favicons-->
    <!--    =============================================-->
    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../assets/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--  -->
    <!--    Stylesheets-->
    <!--    =============================================-->
    <!-- Default stylesheets-->
    <link rel="stylesheet" href="../assets/concat-css/" type="text/css" media="screen">
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
        <?php include('../inc/menu.php'); ?>
        <!-- /.znav-container-->
        <section class="py-0 font-1">
            <div class="container-fluid">
                <div class="row align-items-center text-center justify-content-center h-full">
                    <div class="col-sm-6 col-md-5 col-lg-4 col-xl-3">
                        <h1 class="fw-200 mb-5" align="center">Sign up</h1>
                        <form action="../inc/functions/inscription.php" method="POST">
                            <input class="form-control mb-3" id="nom" name="nom" type="text" placeholder="Nom">
                            <input class="form-control mb-3" id="prenom" name="prenom" type="text" placeholder="Prenom">
                            <input class="form-control mb-3" id="email" name="email" type="email" placeholder="Email">
                            <input class="form-control mb-3" id="pass" name="pass" type="password" placeholder="Mot de passe">
                            <div class="row align-items-center">
                                <div class="col text-right">
                                    <button class="btn-block btn btn-primary" type="submit">Valider</button>
                                </div>
                            </div>
                        </form>
                        <p style="color:red;"><?php echo $status; ?></p>
                        <hr class="color-9 mt-6">
                        <div class="fs--1">Vous avez déja un compte?
                            <a href="connexion">Connectez-vous</a>
                        </div>
                        <hr class="color-9">
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <script src="../assets/lib/jquery/dist/jquery.js"></script>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="../assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Hotjar Tracking Code for http://markup.themewagon.com/tryposh-->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {
                hjid: 588723,
                hjsv: 5
            };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-76729372-3', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="../assets/js/core.js"></script>
    <script src="../assets/js/main.js"></script>
</body>
<!-- Mirrored from markup.themewagon.com/tryposh/page-log-in.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 16:07:35 GMT -->

</html>