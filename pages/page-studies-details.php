<?php
    session_start();
    require('../inc/functions/function.php');
    if(isset($_GET['id'])) $study = findStudiesById($_GET['id']);
    else header('Location:page-studies.php');
    $apropos = getApropos();
    if(isset($_GET['url'])){
        $clean = $study->nom;
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower($clean);
        $clean = preg_replace("/[\/_|+ -]+/", "-", $clean);
        $url = $_GET['url'];
        $true_url = "detail-etudes-".$_GET['id']."-".$clean;
        if($clean != $url){
            header('Location:'.$true_url);
        }
    }
?>

<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryposh/portfolio-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 13:31:31 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  -->
    <!--    Document Title-->
    <!-- =============================================-->
    <title><?php echo $study->nom; ?> | Jose Tahiry Rakotondrazafy</title>
    <!--  -->
    <!--    Favicons-->
    <!--    =============================================-->
    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../assets/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="détail des études de José Tahiry Rakotondrazafy">
    <meta name="keywords" content="CV,José Tahiry,Develloper,Programmer,Etudes">
    <meta name="author" content="José Tahiry">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--  -->
    <!--    Stylesheets-->
    <!--    =============================================-->
    <!-- Default stylesheets-->
    <link rel="stylesheet" href="../assets/concat-css/" type="text/css" media="screen">
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
        <?php include('../inc/menu.php'); ?>
        <!-- /.znav-container-->
        <section class="portfolio font-1">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8">
                        <h1 class="fs-3 fs-md-4 mb-4"><?php echo $study->nom; ?></h1>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col">
                        <img src="../assets/images/<?php echo $study->image; ?>" alt="<?php echo $study->remarque; ?>" width="100%" alt="logo en detail de l'ecole <?php echo $study->image; ?> ">
                    </div>
                </div>
                <div class="row mt-8">
                    <div class="col-md-6 col-lg-4">
                        <div class="mt-4"></div>
                    </div>
                    <div class="col-md-6 col-lg-7 mt-5 mt-md-0">
                        <h2>Descriptions:</h2>
                        <hr class="short left color-8" align="left">
                        <h3 class="color-1 fw-300 color-4"><?php echo $study->descript; ?></h3>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <?php include('../inc/footer.php'); ?>
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <script src="../assets/lib/jquery/dist/jquery.js"></script>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="../assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Hotjar Tracking Code for http://markup.themewagon.com/tryposh-->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {
                hjid: 588723,
                hjsv: 5
            };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-76729372-3', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="../assets/js/core.js"></script>
    <script src="../assets/js/main.js"></script>
</body>
<!-- Mirrored from markup.themewagon.com/tryposh/portfolio-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 13:31:31 GMT -->

</html>