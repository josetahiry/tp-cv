<?php
header("Content-type: text/css; charset=UTF-8");
ob_start('ob_gzhandler');
include "bootstrap.min.css";
include "family-montserrat.css";
include "family-droid-serif.css";
include "font-awesome.min.css";
include "iconsmind/iconsmind.css";
include "hamburgers.css";
include "dropdown.css";
include "transition.css";
include "style.css";
include "custom.css";
ob_end_flush();
?>