create table user(
    id int auto_increment primary key,
    nom varchar(50),
    prenom varchar(50),
    email varchar(50),
    pass varchar(250),
    remarque varchar(50),
	actif tinyint,
	admin tinyint
)engine=InnoDB;

create table categorytext(
    id int auto_increment primary key,
    nom varchar(50) not null,
    descript varchar(100),
    remarque varchar(50)
)engine=InnoDB;

create table texte(
    id int auto_increment primary key,
    idcategory int,
    content varchar(250) not null,
    descript varchar(100),
    remarque varchar(50),
    foreign key (`idcategory`) references `categorytext`(`id`)
)engine=InnoDB;

create table categoryapropos(
    id int auto_increment primary key,
    nom varchar(50) not null,
    descript varchar(100),
    remarque varchar(50)
);

create table apropos(
    id int auto_increment primary key,
    idcategory int,
    content varchar(250) not null,
    descript varchar(100),
    remarque varchar(50),
    foreign key (`idcategory`) references `categoryapropos`(`id`)
)engine=InnoDB;

create table parcours(
    id int auto_increment primary key,
    nom varchar(50) not null,
    datyd int,
    datyf int,
    descript varchar(100),
    remarque varchar(50)
)engine=InnoDB;

create table studies(
    id int auto_increment primary key,
    nom varchar(50) not null,
    datyd int,
    datyf int,
    descript varchar(100),
    remarque varchar(50),
    image varchar(100)
)engine=InnoDB;

create table hobbies(
    id int auto_increment primary key,
    nom varchar(50) not null,
    descript varchar(100),
    remarque varchar(50)
)engine=InnoDB;

create table img(
    id int auto_increment primary key,
    nom varchar(50),
    content blob not null,
    descript varchar(100),
    remarque varchar(50)
)engine=InnoDB;

create table portfolio(
    id int auto_increment primary key,
    nom varchar(50),
    content blob not null,
    idimage int,
    descript varchar(100),
    remarque varchar(50),
    foreign key (`idimage`) references `img`(`id`)
)engine=InnoDB;