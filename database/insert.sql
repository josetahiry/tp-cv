insert into categorytext(nom,descript,remarque) values('home detail','homepage detail',null);

insert into user(nom,prenom,email,pass,remarque,actif,admin) values('Rakoto', 'Tahiry', 'jaydensmock@gmail.com', sha1('root'),'',1,0);
insert into user(nom,prenom,email,pass,remarque,actif,admin) values('Root', 'Root', 'root@gmail.com', sha1('root'),'',1,1);

insert into categoryapropos(nom,descript,remarque) values('Nom','','');
insert into categoryapropos(nom,descript,remarque) values('Prenom','','');
insert into categoryapropos(nom,descript,remarque) values('Email','','');
insert into categoryapropos(nom,descript,remarque) values('Numero','','');

insert into apropos(idcategory,content,descript,remarque) values(1,'Rakotondrazafy','','');
insert into apropos(idcategory,content,descript,remarque) values(2,'Jose Tahiry','','');
insert into apropos(idcategory,content,descript,remarque) values(3,'jose.tahiry.rak@gmail.com','','');
insert into apropos(idcategory,content,descript,remarque) values(4,'+261332654575','','');

insert into texte(idcategory,content,descript,remarque) values(1,'Developpeur','Developpeur et concepteur d application','depuis 2015');
insert into texte(idcategory,content,descript,remarque) values(1,'Web maker & designer','3 ans d experience','depuis 2015');
insert into texte(idcategory,content,descript,remarque) values(1,'Data scientist','Statisticien','depuis 2015');

insert into studies(nom,datyd,datyf,descript,remarque,image) values('Enseignement niveau 2',2013,2015,'Ecole d enseignement generale de niveau 2','enseignement niv2 lspm','studies/lspm.gif');
insert into studies(nom,datyd,datyf,descript,remarque,image) values('Programmation',2015,2018,'Programmation d application desktop et mobile','programmtion itu','studies/itu.jpg');
insert into studies(nom,datyd,datyf,descript,remarque,image) values('Web',2015,2018,'Creation de site web','webdesign itu','studies/itu.jpg');
insert into studies(nom,datyd,datyf,descript,remarque,image) values('Base de donnee',2015,2018,'Administration et analyse de donnee','base de donnee itu','studies/itu.jpg');
insert into studies(nom,datyd,datyf,descript,remarque,image) values('Reseau',2015,2018,'Administration reseau','reseau itu','studies/itu.jpg');

insert into parcours(nom,datyd,datyf,descript,remarque) values('Mathematiques',2015,2017,'Analyse, Calculs vectoriels et matriciels','');
insert into parcours(nom,datyd,datyf,descript,remarque) values('Developpement de programmes de gestion',2016,2018,'Programme de gestion d entreprise et de comptabilite','');
insert into parcours(nom,datyd,datyf,descript,remarque) values('Developpement de mini-jeu',2016,2018,'Mini jeu en 2d et en 3d','');
insert into parcours(nom,datyd,datyf,descript,remarque) values('Conception de site web',2015,2018,'Site web dynamique a interface responsive, sous differentes technologies','');
insert into parcours(nom,datyd,datyf,descript,remarque) values('Developpement d application mobile',2017,2018,'Sous la plateforme hybride, html5, css3, anguler4/5','');
insert into parcours(nom,datyd,datyf,descript,remarque) values('Application reseau',2016,2017,'Application de partage de fichier en reseau','');

insert into hobbies(nom,descript,remarque) values('Football','','');
insert into hobbies(nom,descript,remarque) values('Basketball','','');
insert into hobbies(nom,descript,remarque) values('Natation','','');